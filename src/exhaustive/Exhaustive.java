/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exhaustive;

/**
 *
 * @author Karntima
 */
public class Exhaustive {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int number = 3;
        printAllBinary(number);
    }

    public static void printAllBinaryHelper(int number, String s) {
        if (number == 0) {
            System.out.print(s + " " + "\n");
        } else {
            printAllBinaryHelper(number - 1, s + "0");
            printAllBinaryHelper(number - 1, s + "1");
        }
    }

    public static void printAllBinary(int number) {
        printAllBinaryHelper(number, " ");
    }

}
